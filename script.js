const filesList = [
    {
        name: 'file 1',
        size: 10,
        response: false
    },
    {
        name: 'file 2',
        size: 12,
        response: true
    },
    {
        name: 'file 3',
        size: 4,
        response: false
    },
    {
        name: 'file 4',
        size: 4,
        response: true
    },
    {
        name: 'file 5',
        size: 7,
        response: false
    },
    {
        name: 'file 6',
        size: 7,
        response: false
    },
    {
        name: 'file 7',
        size: 2,
        response: true
    },
    {
        name: 'file 8',
        size: 2,
        response: false
    },
    {
        name: 'file 9',
        size: 1,
        response: false
    },
    {
        name: 'file 10',
        size: 1,
        response: true
    }
];

function uploadFiles(filesList, callback) {
    filesList.map((file) => {
        let timerId = setTimeout(() => {
            clearTimeout(timerId);
            callback(file.name, file.size, file.response);
        }, file.size * 1000)
    })
}

uploadFiles(filesList, function(name, size, response){
    console.log( 'File: ', name, ' uploaded ', 'size:' , size);
    response ? console.info('%cВсё прекрасно! ОК 200', 'color: green') : console.error('Not OK! Wrong format')
})
